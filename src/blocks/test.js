import * as Blockly from 'blockly/core';

export const block_test = {
    init: function () {
        this.appendDummyInput()
            .appendField(Blockly.Msg.MIXLY_T_BOARD_HELLO_MIXLY_TEXT);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setColour('#1296db');
        this.setTooltip("");
        this.setHelpUrl("");
    }
};
