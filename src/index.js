import * as Blockly from 'blockly/core';
import Variables from './others/variables';
import { Javascript } from './generators/javascript_generator';
import * as jsTestBlocks from './blocks/test';
import * as jsTestGenerators from './generators/test';
import './language/loader';
import './css/color.css';

Blockly.Javascript = Javascript;
Object.assign(Blockly.Variables, Variables);

// 载入图形化模块外观定义文件
Object.assign(
    Blockly.Blocks,
    jsTestBlocks
);

// 载入图形化模块代码生成定义文件
Object.assign(
    Blockly.Javascript.forBlock,
    jsTestGenerators
);